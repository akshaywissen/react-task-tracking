import axios from "axios";
import moment from "moment";
import { 
    ADD_TASK_QUERY, 
    GET_TASK_LIST_QUERY, 
    GET_TASK_QUERY, 
    TASK_TAG_INSERT_QUERY, 
    TASK_TAG_DELETE_QUERY, 
    UPDATE_TASK_QUERY, 
    DELETE_TASK_QUERY 
} from "../gql.queries"

import {
    ROOT_URL,
    ACCESS_TOKEN
} from '../config'

export const FETCH_TASKS = "FETCH_TASKS";
export const FETCH_TASK = "FETCH_TASK";
export const CREATE_TASK = "CREATE_TASK";
export const UPDATE_TASK = "UPDATE_TASK";
export const DELETE_TASK = "DELETE_TASK";

axios.interceptors.request.use(function (config) {
    document.body.classList.add('loading-indicator');
    config.headers.Authorization = `Bearer ${ ACCESS_TOKEN }`
    return config
}, function (error) {
    return Promise.reject(error);
});
  
axios.interceptors.response.use(function (response) {
    document.body.classList.remove('loading-indicator');
    if(response.status !== 200) {
        alert('There is some error! Please check your token!')
        return;
    }
    return response.data.data;
}, function (error) {
    return Promise.reject(error);
});


/**
 * Action to fetch Tasks list
 */
export function fetchTasks(condition = null) {
    const query = GET_TASK_LIST_QUERY, type = FETCH_TASKS;
    const where = {};
    if(condition) {
        if(condition.title) {
            where.title = {
                _ilike: condition.title
            }
        }
        if(condition.start_time) {
            where.start_time = {
                _gte: moment(condition.start_time).format('YYYY-MM-DD')
            }
        }
        if(condition.end_time) {
            where.end_time = {
                _lte: moment(condition.end_time).format('YYYY-MM-DD')
            }
        }
    }
    return _makeGraphQLRequest({ query, type, variables: { filter: where } });
}

/**
 * Api request action to create the task
 * @param {Object} values 
 * @param {Function} callback 
 */
export function createTask(values, callback) {

    const on_tag_conflict = {
        constraint: "tags_name_user_id_key",
        update_columns: ["name"]
    }

    const tagsObjectArray = values.tags.map(tag => {
        return {
            tag: {
                data: {
                    name: tag
                },
                on_conflict: on_tag_conflict
            }
        }
    })

    const variables = {
        title: values.title,
        tags: tagsObjectArray
    }

    const query = ADD_TASK_QUERY, type = CREATE_TASK;
    return _makeGraphQLRequest({ query, variables, type, callback });

}

/**
 * API action to fetch a particular task
 * @param {Number|String} id 
 * @param {Function} callback 
 */
export function fetchTask(id, callback) {
    const query = GET_TASK_QUERY, type = FETCH_TASK;
    const variables = { id }
    return _makeGraphQLRequest({ query, variables, type, callback });
}

/**
 * API action to delete a particular task
 * @param {Number|String} id 
 * @param {Function} callback 
 */
export function deleteTask(id, callback) {
    const query = DELETE_TASK_QUERY, type = DELETE_TASK;
    const variables = { id }
    const deleteTaskCallback = () => _deleteTaskTags(id, callback);
    return _makeGraphQLRequest({ query, variables, type, callback: deleteTaskCallback });
}

/**
 * API action to update a particular task
 * @param {Number|String} id 
 * @param {Object} values 
 * @param {Function} callback 
 */
export function updateTask(id, values, callback) {
    const query = UPDATE_TASK_QUERY, type = UPDATE_TASK;
    const updateObject = {};
    
    if(values.title) {
        updateObject.title = values.title;
    }
    if(values.start_time) {
        updateObject.start_time = values.start_time;
    }
    if(values.end_time) {
        updateObject.end_time = values.end_time;
    }

    const variables = {
        updateObject,
        condition: {
            id
        }
    }

    let updateTaskCallback;
    if(values.tags && values.tags.length) {
        updateTaskCallback = () => {
            const deleteTaskCallback = () => _insertTaskTags({ taskId: id, tags: values.tags, callback });
            _deleteTaskTags(id, deleteTaskCallback);
        }
    } else {
        updateTaskCallback = callback;
    }

    return _makeGraphQLRequest({ query, variables, type, callback: updateTaskCallback });
}

/**
 * Insert tags for taskId
 * @param {Object} param0 
 */
function _insertTaskTags({ taskId, tags = [], callback }) {
    const query = TASK_TAG_INSERT_QUERY;
    const insertArray = tags.map(tag => {
        return {
            task_id: taskId,
            tag: {
                data: {
                    name: tag
                },
                on_conflict: {
                    constraint: "tags_name_user_id_key",
                    update_columns: ["name"]
                }
            }
        }
    })

    const variables = {
        insertArray,
        conflictObj: {
          constraint: "task_tag_task_id_tag_id_key",
          update_columns: []
        }
    }
    return _makeGraphQLRequest({ query, variables, callback })
}

/**
 * delete tags for taskId
 * @param {Number|String} taskId 
 */
function _deleteTaskTags(taskId, callback = null) {
    const query = TASK_TAG_DELETE_QUERY;
    
    const variables = {
        condition: {
          task_id: {
              _eq: taskId
          }
        }
    }
    return _makeGraphQLRequest({ query, variables, callback })
}

/**
 * make the axios request to graphQL
 * @param {Object} param0 
 */
function _makeGraphQLRequest({ query, variables = {}, type = null, callback = null }) {
    const request = axios
        .post(`${ROOT_URL}`, {
            query,
            variables
        })
    if(callback) {
        console.log('here')
        request.then(() => callback())
    }
    if(type) {
        return {
            type,
            payload: request
        };
    }
    return request;
}