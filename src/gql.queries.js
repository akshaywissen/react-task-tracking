export const ADD_TASK_QUERY = `
mutation insert_tasks_one($title: String!, $tags:[task_tag_insert_input!]!) {
    insert_tasks_one(object: {
        title: $title,
        task_tags: {
            data: $tags
        }
    }){
      created_at
      end_time
      id
      start_time
      tags {
        id,
        name
      }
      title
      updated_at
    }
}`;

export const GET_TASK_LIST_QUERY = `
query getTasks($filter: tasks_bool_exp ){
    tasks(
        order_by: { updated_at: desc }
        where: $filter
    ){
      created_at
      end_time
      id
      start_time
      tags {
        id,
        name
      }
      title
      updated_at
    }
}`;

export const GET_TASK_QUERY = `
query tasks_by_pk($id: Int!){
    tasks_by_pk(id: $id){
      created_at
      end_time
      id
      start_time
      tags {
        id,
        name
      }
      title
      updated_at
    }
}`;

export const UPDATE_TASK_QUERY = `
mutation update_tasks_by_pk($updateObject: tasks_set_input, $condition: tasks_pk_columns_input!){
    update_tasks_by_pk(_set: $updateObject, pk_columns: $condition){
      created_at
      end_time
      id
      start_time
      title
      updated_at
    }
}`

export const DELETE_TASK_QUERY = `
mutation delete_tasks_by_pk($id: Int!){
    delete_tasks_by_pk(id: $id){
      created_at
      end_time
      id
      start_time
      title
      updated_at
    }
}`

export const TASK_TAG_DELETE_QUERY = `
mutation delete_task_tag($condition: task_tag_bool_exp!){
    delete_task_tag(where: $condition){
      affected_rows
    }
}`;

export const TASK_TAG_TASK_ID_TAG_ID_KEY = `task_tag_task_id_tag_id_key`;

export const TASK_TAG_INSERT_QUERY = `
mutation insert_task_tag($insertArray: [task_tag_insert_input!]!, $conflictObj: task_tag_on_conflict){
    insert_task_tag(objects: $insertArray, on_conflict: $conflictObj){
      affected_rows
    }
}`

