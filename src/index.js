import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import promise from "redux-promise";

import reducers from "./reducers";
import TaskList from "./components/TaskList/task-list.component";
import { TaskAddComponent, TaskEditComponent } from "./components/TaskAdd/task-add.component";
import Header from "./components/Header/header.component";

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
      <div>
        <Header />
        <Switch>
          <Route path="/tasks/new" component={TaskAddComponent} />
          <Route path="/tasks/:id" component={TaskEditComponent} />
          <Route path="/" component={TaskList} />
        </Switch>
      </div>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);