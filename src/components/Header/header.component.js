import React from 'react'
import Typography from '@material-ui/core/Typography';

import './header.component.css';
const Header = () => (
    <div className="app">
        <header className="app-header">
            <Typography component="h1" variant="h5">
                React Sample
            </Typography>
        </header>
    </div>
)


export default Header