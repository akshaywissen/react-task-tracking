import React, { Component } from "react";
import { connect } from "react-redux";

import Timer from "../Timer/timer.component";
import { Link } from "react-router-dom";
import moment from 'moment';

import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Chip from '@material-ui/core/Chip';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { deleteTask, updateTask } from "../../actions";

class Task extends Component {

    titleCase(text) {
        return text.charAt(0).toUpperCase() + text.substr(1).toLowerCase();
    }

    deleteTask(taskId) {
        this.props.deleteTask(taskId, () => {
            this.props.callbackFunction();
        })
    }

    updateTaskStatus(id, flag) {
        const updateObj = {};
        if(flag === 'start') {
          updateObj.start_time = new Date();
        } else {
          updateObj.end_time = new Date();
        }
        this.props.updateTask(id, updateObj, () => {
          this.props.callbackFunction();
        })
    }
    
    formatDate(dateString) {
        return moment(dateString).format('MM YYYY, hh:mm A')
    }

    
    render() {
        const { task } = this.props;
        return (
            <Card className="task-card" >
                <CardHeader
                title={
                    <Typography variant="h5" component="h2" color="primary">
                    { this.titleCase(task.title) }
                    </Typography>
                }
                subheader={ moment(task.created_at).fromNow() }
                action={
                    !task.start_time || task.end_time ?
                        <div>
                            <IconButton 
                                aria-label="edit"
                                component={ Link }
                                to={ `/tasks/${ task.id }` }
                            >
                                <Edit />
                            </IconButton>
                            <IconButton 
                                aria-label="delete"
                                onClick={ () => this.deleteTask(task.id) }
                            >
                                <Delete />
                            </IconButton>
                        </div>
                        :
                        null
                }
                />
                
                <CardContent>
                <Typography className="timer-display" variant="h5" component="h2">
                    <Timer startTime={ task.start_time } endTime={ task.end_time } />
                </Typography>
                </CardContent>
                
                <CardContent>
                {
                    task.tags.map((tag, index) => <Chip className="task-tags" key={ index } size="small" label={ this.titleCase(tag.name) } />)
                }
                </CardContent>
                
                <CardActions>
                {
                    task.start_time ?
                    (
                    !task.end_time ?
                    <Button color="secondary" size="small" onClick={ () => { this.updateTaskStatus(task.id, 'end') } }>End</Button>
                    :
                    <Typography className="task-status" color="textSecondary">
                        Completed on { this.formatDate(task.end_time) }
                    </Typography>
                    )
                    :
                    <Button color="primary" size="small" onClick={ () => { this.updateTaskStatus(task.id, 'start') } }>Start</Button>
                }
                </CardActions>
            </Card>
        )
    }
}


export default connect(null, { deleteTask, updateTask })(Task);