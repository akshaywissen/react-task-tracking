import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

import Task from '../Task/task.component'
import './task-list.component.css';
import { fetchTasks } from "../../actions";

class TaskList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showFilters: false,
      filters: {
        start_time: null,
        end_time: null,
        title: ''
      }
    }
  }

  setFilters(filterKey, value) {
    const { filters } = this.state;
    filters[filterKey] = value;
    this.setState({ filters })
  }

  componentDidMount() {
    this.props.fetchTasks();
  }

  searchTask() {
    const { filters } = this.state;
    this.props.fetchTasks(filters);
  }

  clearFilters() {
    const filters = {
      start_time: null,
      end_time: null,
      title: ''
    }
    this.setState({ filters })
  }

  refetchList() {
    // this.props.fetchTasks();
    window.location.reload()
  }


  render() {
    const { showFilters } = this.state;

    return (
      <div className="list-wrapper">
        <div className="list-wrapper-head">
          <Typography component="h2" variant="h4">
            Task List
          </Typography>
          
          <Button
            className="add-button"
            type="button"
            variant="contained"
            color="primary"
            component={ Link }
            to="/tasks/new"
          >
            Add Task
          </Button>

          <Button
            type="button"
            variant="contained"
            color="secondary"
            onClick={() => this.setState({ showFilters: !showFilters })}
          >
            Filters
          </Button>
        </div>

        { 
          showFilters ?
          <div className="list-wrapper-filter">
            <TextField
              className="filter-field"
              label="Start Date"
              type="date"
              variant="outlined"
              onChange={ event => this.setFilters('start_time', event.target.value) }
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              className="filter-field"
              label="End Date"
              type="date"
              variant="outlined"
              defaultValue={ null }
              InputLabelProps={{
                shrink: true,
              }}
              onChange={ event => this.setFilters('end_time', event.target.value) }
            />
            <TextField
              className="filter-field"
              id="search"
              label="Search"
              type="search"
              variant="outlined"
              onChange={ event => this.setFilters('title', event.target.value) }
            />
            <Button
              type="button"
              variant="contained"
              color="primary"
              onClick={() => this.searchTask() }
            >
              Apply
            </Button>
            
          </div>
          : ''
        }

        <Grid container spacing={3}>
          {
            this.props.taskList.length > 0 ?
              this.props.taskList.map((task, index) => (
                <Grid item xs={4} key={ index }>
                  <Task callbackFunction={ this.refetchList.bind(this) } task={ task } />
                </Grid>
              ))
              :
              <Typography gutterBottom variant="h6" component="h6">
                  No Tasks Added!
              </Typography>
          }
        </Grid>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { taskList } = state.task;
  return { taskList };
}

export default connect(mapStateToProps, { fetchTasks })(TaskList);