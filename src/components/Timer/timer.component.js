import { Component } from 'react'
import moment from 'moment';

export default class Timer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            formatted: '00:00:00',
        }
    }

    componentDidMount() {
        this.startTimer();
    }

    componentWillUnmount() {
        if (this.invteral)
            clearInterval(this.intveral);
    }

    padZero(val) {
        return val > 9 ? val : "0" + val;
    }


  startTimer() {
    const { startTime = null, endTime = null } = this.props;

    if(startTime) {
        if(endTime) {
            this.calculateDifference(startTime, endTime);
        } else {
            this.invteral = setInterval(() => {
                this.calculateDifference(startTime);
            }, 1000)
        }
    }
  }

  stopTimer() {
    if(this.invteral)
        clearInterval(this.intveral);
  }


  calculateDifference(startTime, endTime = new Date()) {
    const milliSeconds = moment(new Date(endTime)).diff(moment(new Date(startTime)));
    const duration = moment.duration(milliSeconds);
    const formatted = `${ this.padZero(duration.hours()) }:${ this.padZero(duration.minutes()) }:${ this.padZero(duration.seconds()) }`;
    this.setState({ formatted });
  }


  render() {
    return this.state.formatted;
  }
}
