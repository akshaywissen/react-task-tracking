import React, { Component } from "react";
import { Field, FieldArray, reduxForm, arrayPush } from "redux-form";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import IconButton from '@material-ui/core/IconButton';
import Add from '@material-ui/icons/Add';
import Remove from '@material-ui/icons/Remove';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';

import { createTask, fetchTask, updateTask } from "../../actions";
import './task-add.component.css';
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}




const renderTextField = (field) =>{
  const {
    input,
    label,
    id,
    className,
    meta: { touched, error }
  } = field;
  return (
    <TextField
      variant="outlined"
      margin="normal"
      required
      id={ id }
      className={ className }
      label={ label }
      error={ touched && error ? true : false }
      helperText={ touched && error }
      {...input}
    />
  )
}

const renderTagsField = (fieldArr) => {
  const { fields } = fieldArr;
  
  return (
    fields.map((tag, index) => {
      return (
        <div key={ index } className="form-field">
          <Field
            className="form-field-single"
            name={ tag }
            id={ `tag${ index + 1 }` }
            component={ renderTextField }
            label={ `Tag ${ index + 1}` }
          />
          { 
            index === 0 ? 
            <IconButton onClick={ () => fields.push() } className="icon-button" size="medium" color="primary" aria-label="upload picture" component="span">
              <Add />
            </IconButton>
            :
            <IconButton onClick={ () => fields.remove(index) } className="icon-button" size="medium" color="primary" aria-label="upload picture" component="span">
              <Remove />
            </IconButton>
          }
        </div>
      )
    })
  )
  
}


class TaskAdd extends Component {

  state = {
    toast: {
      showToast: false,
      toastMessageSeverity: 'success',
      toastMessage: ''
    }
  }

  componentDidMount() {
    const { id = null } = this.props.match.params;
    if(id) {
      this.props.fetchTask(id);
    }
  }

  componentWillMount() {
    this.props.initialize({ tags: [""] });
  }

  _showSuccessMessage() {
    const toast = {
      showToast: true,
      toastMessageSeverity: 'success',
      toastMessage: 'Action performed successfully'
    }
    this.setState({ toast })
  }
  
  onSubmit(values) {
    const { id = null } = this.props.match.params;
    const { createTask, updateTask } = this.props;
    if(id) {
      updateTask(id, values, () => {
        this._showSuccessMessage();
        this.props.history.push("/");
      })
    } else {
      createTask(values, () => {
        this._showSuccessMessage();
        this.props.history.push("/");
      });
    }
  }

  render() {
    const { handleSubmit, pristine, submitting } = this.props;
    
    return (
      <Container component="main" maxWidth="sm">
        <CssBaseline />
        <div className="paper">
          <Typography component="h1" variant="h5">
            Add Task
          </Typography>
          <form method="POST" className="form" noValidate onSubmit={ handleSubmit(this.onSubmit.bind(this)) }>
            <div className="form-field">
              <Field
                className="form-field-single"
                id="title"
                name="title"
                component={ renderTextField }
                label="Task Title"
              />
            </div>

            <FieldArray name="tags" component={ renderTagsField }/>

            <div className="form-buttons-container">
              <Button
                className="form-buttons"
                type="submit"
                variant="contained"
                color="primary"
                disabled={ pristine || submitting }
              >
                Submit
              </Button>

              <Button
                className="form-buttons"
                type="button"
                variant="contained"
                color="secondary"
                component={ Link }
                to="/"
              >
                Cancel
              </Button>
            </div>
          </form>
        </div>
        <Snackbar open={ this.state.showToast } autoHideDuration={3000}>
          <Alert severity={ this.state.toastMessageSeverity }>
            { this.state.toastMessage }
          </Alert>
        </Snackbar>
      </Container>
    )
  }
}

const mapDispatchToProps = {
  createTask,
  updateTask,
  fetchTask,
  pushArray: arrayPush
};

function mapStateToProps(state) {
  const { selectedTask } = state.task;
  
  return { 
    selectedTask,
    initialValues: selectedTask
  };
}

function validate(values) {
  const errors = {};

  if (!values.title) {
    errors.title = 'Required'
  }
  
  const tagErrors = [];
  if(values.tags && values.tags.length) {
    values.tags.forEach((tag, index) => {
      if(!tag) {
        tagErrors[index] = 'Required'
      }
    })
    if(tagErrors.length)
      errors.tags = tagErrors;
  }
  return errors;
}

TaskAdd = reduxForm({
  form: "TaskAddForm",
  validate,
})(TaskAdd)

export const TaskAddComponent = connect(mapStateToProps, mapDispatchToProps)(TaskAdd);


const TaskEdit = reduxForm({
  form: "TaskEditForm",
  validate,
  enableReinitialize: true,
})(TaskAdd)

export const TaskEditComponent = connect(mapStateToProps, mapDispatchToProps)(TaskEdit);

