import { combineReducers } from 'redux';
import task from './task.reducer';
import { reducer as formReducer } from 'redux-form'

export default combineReducers({
    task,
    form: formReducer
})