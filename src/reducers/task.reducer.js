import { FETCH_TASKS, FETCH_TASK } from "../actions";

const initialState = {
    taskList: [],
    selectedTask: {}
}

export default function(state = initialState, action) {
    
    const { type } = action;
    switch (type) {
        case FETCH_TASKS:
            const taskList = action.payload.tasks;
            return { ...state, taskList };

        case FETCH_TASK:
            const task = action.payload.tasks_by_pk;
            task.tags = task.tags.map(tag => tag.name);
            if(task.tags.length === 0) {
                task.tags.push("");
            }
            const selectedTask = task;
            return { ...state, selectedTask }

        default:
            return state;
    }
}
