This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm install`

Get your token by login:

`https://test-323-c4fca.web.app/` and copy your_token to `ACCESS_TOKEN` in `./src/config.js`

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
